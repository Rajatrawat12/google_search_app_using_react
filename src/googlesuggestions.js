import logo from "./logo.svg";
import "./googlesuggestions.css";
import Suggestions from "./components/suggestions";
import { useEffect, useState } from "react";
function App() {
  const suggestionList = [
    {
      id: 1,
      suggestion: "price of Diesal",
    },
    {
      id: 2,
      suggestion: "Oculus Quest 2 space",
    },
    {
      id: 3,
      suggestion: "Tesla share price",
    },
    {
      id: 4,
      suggestion: "price of Ethereum today",
    },
    {
      id: 5,
      suggestion: "Latest trends in AI",
    },
    {
      id: 6,
      suggestion: "Latest trends in ML",
    },
  ];
  const [updatedsuggestionlist, setUpdatedSuggestionList] =
    useState(suggestionList);
  const [searchinput, setSearchInputItem] = useState("");

  const onChangeInput = (event) => {
    setSearchInputItem(event.target.value);
  };

  const updatedlist = () => {
    const filteredlist = suggestionList.filter((each) => {
      return each.suggestion.includes(searchinput);
    });
    return filteredlist;
  };

  useEffect(() => {
    setUpdatedSuggestionList(updatedlist());
  }, [searchinput]);

  const onArrowClick = (id) => {
    const clickedid = suggestionList.filter((each) => {
      return each.id === id;
    });
    // console.log("Clicked",clickedid[0].suggestion);
    setSearchInputItem(clickedid[0].suggestion);
  };

  return (
    <div className="maincontainer">
      <div className="googlecontainer">
        <img
          className="googlelogo"
          src="https://assets.ccbp.in/frontend/react-js/google-logo.png"
        />
      </div>
      <div className="bulgcontainer">
        <div className="searchcontainer">
          <img
            className="searchicon"
            src="https://assets.ccbp.in/frontend/react-js/google-search-icon.png"
          />
          <input
            type="search"
            placeholder="Search Google"
            className="searchtextcontainer"
            onChange={onChangeInput}
            value={searchinput}
          />
        </div>
        {updatedsuggestionlist.map((eachItem) => {
          return (
            <Suggestions eachItem={eachItem} onArrowClick={onArrowClick} />
          );
        })}
      </div>
    </div>
  );
}
export default App;
