import "./suggestions.css";

const Suggestions = (props) => {
  const { eachItem, onArrowClick } = props;
  const { id, suggestion } = eachItem;

  // onclick=()=>{
  //   onArrowClick(id)
  // }

  return (
    <>
      <div className="suggestionitemscontainer">
        <p className="searchtext">{suggestion}</p>
        <img
          className="arrowicon"
          src="https://assets.ccbp.in/frontend/react-js/diagonal-arrow-left-up.png "
          onClick={() => onArrowClick(id)}
        />
      </div>
    </>
  );
};

export default Suggestions;
